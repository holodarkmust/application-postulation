﻿
namespace Postulation_formation
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_moi = new System.Windows.Forms.Button();
            this.btn_envieParticipation = new System.Windows.Forms.Button();
            this.btn_centresInterets = new System.Windows.Forms.Button();
            this.btn_formation = new System.Windows.Forms.Button();
            this.btn_interetCyber = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_moi
            // 
            this.btn_moi.Location = new System.Drawing.Point(12, 12);
            this.btn_moi.Name = "btn_moi";
            this.btn_moi.Size = new System.Drawing.Size(81, 40);
            this.btn_moi.TabIndex = 0;
            this.btn_moi.Text = "Moi";
            this.btn_moi.UseVisualStyleBackColor = true;
            this.btn_moi.Click += new System.EventHandler(this.btn_moi_Click);
            // 
            // btn_envieParticipation
            // 
            this.btn_envieParticipation.Location = new System.Drawing.Point(70, 77);
            this.btn_envieParticipation.Name = "btn_envieParticipation";
            this.btn_envieParticipation.Size = new System.Drawing.Size(81, 58);
            this.btn_envieParticipation.TabIndex = 1;
            this.btn_envieParticipation.Text = "Pourquoi je souhaite participer";
            this.btn_envieParticipation.UseVisualStyleBackColor = true;
            this.btn_envieParticipation.Click += new System.EventHandler(this.btn_envieParticipation_Click);
            // 
            // btn_centresInterets
            // 
            this.btn_centresInterets.Location = new System.Drawing.Point(245, 12);
            this.btn_centresInterets.Name = "btn_centresInterets";
            this.btn_centresInterets.Size = new System.Drawing.Size(81, 40);
            this.btn_centresInterets.TabIndex = 2;
            this.btn_centresInterets.Text = "Mes centres d\'intérêts";
            this.btn_centresInterets.UseVisualStyleBackColor = true;
            this.btn_centresInterets.Click += new System.EventHandler(this.btn_centresInterets_Click);
            // 
            // btn_formation
            // 
            this.btn_formation.Location = new System.Drawing.Point(136, 12);
            this.btn_formation.Name = "btn_formation";
            this.btn_formation.Size = new System.Drawing.Size(81, 40);
            this.btn_formation.TabIndex = 3;
            this.btn_formation.Text = "Ma formation";
            this.btn_formation.UseVisualStyleBackColor = true;
            this.btn_formation.Click += new System.EventHandler(this.btn_formation_Click);
            // 
            // btn_interetCyber
            // 
            this.btn_interetCyber.Location = new System.Drawing.Point(186, 77);
            this.btn_interetCyber.Name = "btn_interetCyber";
            this.btn_interetCyber.Size = new System.Drawing.Size(86, 58);
            this.btn_interetCyber.TabIndex = 4;
            this.btn_interetCyber.Text = "Intérêts pour le domaine cyber";
            this.btn_interetCyber.UseVisualStyleBackColor = true;
            this.btn_interetCyber.Click += new System.EventHandler(this.btn_interetCyber_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(341, 148);
            this.Controls.Add(this.btn_interetCyber);
            this.Controls.Add(this.btn_formation);
            this.Controls.Add(this.btn_centresInterets);
            this.Controls.Add(this.btn_envieParticipation);
            this.Controls.Add(this.btn_moi);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Postulation Armée";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_moi;
        private System.Windows.Forms.Button btn_envieParticipation;
        private System.Windows.Forms.Button btn_centresInterets;
        private System.Windows.Forms.Button btn_formation;
        private System.Windows.Forms.Button btn_interetCyber;
    }
}

