﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Postulation_formation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void btn_moi_Click(object sender, EventArgs e)
        {
            FormPrésentation formMoi = new FormPrésentation();
            formMoi.ShowDialog();
        }

        private void btn_formation_Click(object sender, EventArgs e)
        {
            FormFormation formFormation = new FormFormation();
            formFormation.ShowDialog();
        }

        private void btn_centresInterets_Click(object sender, EventArgs e)
        {
            FormCentresInterets formCentresInterets = new FormCentresInterets();
            formCentresInterets.ShowDialog();
        }

        private void btn_envieParticipation_Click(object sender, EventArgs e)
        {
            FormSouhaitParticipation formSouhaitParticipation = new FormSouhaitParticipation();
            formSouhaitParticipation.ShowDialog();
        }

        private void btn_interetCyber_Click(object sender, EventArgs e)
        {
            FormInteretCyber formInteretCyber = new FormInteretCyber();
            formInteretCyber.ShowDialog();
        }
    }
}
